$(function(){
     $.ajax({
        url: "model/retorna-posts.php",
        type: "POST",
        processData: false,
        dataType: "json",
        cache: false,
        contentType: false,
        success: function( data ) {	
            var corpo = document.getElementById("corpo");
            console.log(data[0]);
            
            for(var  i = 0 ; i < data.length ; i++){
                let post = document.createElement("div");
                $(post).addClass("post");
                
                let titulo = document.createElement("h1");
                titulo.innerHTML = data[i].titulo;
                $(titulo).addClass("post-title");
                
                let texto = document.createElement("div");
                texto.innerHTML = data[i].texto;
                $(texto).addClass("post-texto");
                
                let autor = document.createElement("div");
                autor.innerHTML = data[i].autor;
                $(autor).addClass("post-autor");
                
                let dt = document.createElement("div");
                dt.innerHTML = data[i].data;
                $(dt).addClass("post-data");
                
                
                post.appendChild(titulo);
                post.appendChild(texto);
                post.appendChild(autor);
                post.appendChild(dt);
                
                corpo.appendChild(post);    
            }
        },
        error: function (request, status, error) {

        }
    });  
});