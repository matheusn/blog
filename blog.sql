-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2018 at 06:32 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `titulo` varchar(30) NOT NULL,
  `texto` text NOT NULL,
  `autor` varchar(30) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`titulo`, `texto`, `autor`, `data`) VALUES
('Bolsonaro quer aumentar maiori', 'O candidato Jair Bolsonaro farÃ¡ alteraÃ§Ãµes em seu projeto sobre a maioridade penal. Bolsonaro vai aumentar o limite de responsabilidade para 34 anos. A decisÃ£o foi tomada depois que seu filho Eduardo Bolsonaro dizer que pare fechar o STF basta um jipe e um cabo.\r\n\r\nBolsonaro disse afirmou que Eduardo foi advertido. â€œJÃ¡ adverti o garotoâ€, disse ele. Logo depois, o candidato se reuniu com assessores para fazer alteraÃ§Ãµes na lei.\r\n\r\nO candidato depois pediu desculpas aos juÃ­zes. NÃ£o houve, porÃ©m, pedido de desculpas a gays, mulheres e negros.', 'O Sensacionalista', '2018-10-13'),
('Petista que estava em coma aco', 'O petista Waldomiro Ferreira despertou ontem de um coma de 20 dias justamente na hora em que Haddad dava entrevista para o SBT. Ao ver o petista elogiando Moro, Waldomiro pediu pelo amor de Deus para apagar novamente.\r\n\r\nInicialmente Waldomiro teve uma crise achando que estava ouvindo coisas. Agitado, ele precisou ser contido pelos parentes.\r\n\r\nA coisa ficou pior ainda quando mostraram a Waldomiro o material de campanha verde e amarelo.\r\n\r\nMas Waldomiro nem precisou ser forÃ§ado a apagar. Ele perguntou pelo Ibope e hibernou novamente. Pouco antes, pediu para sÃ³ ser incomodado daqui a quatro anos.', 'O Sensacionalista', '2018-10-20');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
